#include <QApplication>

#include "sd/controller.h"
#include "sd/models/toolbox_model.h"
#include "sd/scene/scene.h"
#include "sd/widgets/main_window.h"

int main(int argc, char** argv) {
  QApplication app(argc, argv);
  sd::ToolboxModel toolboxModel(nullptr);
  sd::Scene scene(nullptr);
  sd::Controller controller(nullptr, scene);

  QObject::connect(&scene, &sd::Scene::undo,
                   [&controller]() { controller.undo(); });
  QObject::connect(&scene, &sd::Scene::redo,
                   [&controller]() { controller.redo(); });

  controller.addState(nullptr, scene.sceneRect().center());

  sd::MainWindow window(toolboxModel, scene, nullptr);
  window.show();

  return app.exec();
}
