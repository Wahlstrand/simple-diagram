#pragma once

#include <QAbstractListModel>
#include <vector>

namespace sd {
class ToolboxModel : public QAbstractListModel {
  Q_OBJECT
 public:
  explicit ToolboxModel(QObject *pParent);

  int rowCount(const QModelIndex &parent) const override;
  int columnCount(const QModelIndex &parent) const override;
  QVariant data(const QModelIndex &index, int role) const override;
  Qt::ItemFlags flags(const QModelIndex &index) const override;

  Qt::DropActions supportedDragActions() const override;
  QMimeData *mimeData(const QModelIndexList &indexes) const override;

 private:
  std::vector<QString> m_actions;
};
}  // namespace sd