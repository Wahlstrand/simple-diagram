#pragma once

#include <QObject>
#include <QUndoStack>

#include "sd/scene/state_graphics_item.h"

namespace sd {

class Controller : public QObject {
  Q_OBJECT
 public:
  explicit Controller(QObject* pParent, QGraphicsScene& scene);

  Q_SLOT void undo();
  Q_SLOT void redo();
  Q_SLOT void addState(StateGraphicsItem* pParent, QPointF pos);

 private:
  QUndoStack m_undoRedo;
  QGraphicsScene& m_scene;
};
}  // namespace sd