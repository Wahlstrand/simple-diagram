#pragma once

#include <QGraphicsScene>

namespace sd {
class Scene : public QGraphicsScene {
  Q_OBJECT
 public:
  explicit Scene(QObject *pParent);

  // drag-drop
  void dragEnterEvent(QGraphicsSceneDragDropEvent *pEvent) override;
  void dragLeaveEvent(QGraphicsSceneDragDropEvent *pEvent) override;
  void dragMoveEvent(QGraphicsSceneDragDropEvent *pEvent) override;
  void dropEvent(QGraphicsSceneDragDropEvent *pEvent) override;

  // keyboard
  void keyPressEvent(QKeyEvent *pKeyEvent) override;

  Q_SIGNAL void undo();
  Q_SIGNAL void redo();
};
}  // namespace sd