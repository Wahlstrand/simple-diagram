#pragma once

#include <QGraphicsObject>

namespace sd {

enum class DragMode { none, top_right, bottom_right, bottom_left, top_left };

class StateGraphicsItem : public QGraphicsObject {
  Q_OBJECT
 public:
  explicit StateGraphicsItem(QRectF renderRect, QGraphicsObject *pParent);

  // Painting
  QRectF boundingRect() const override;
  void paint(QPainter *pPainter, const QStyleOptionGraphicsItem *option,
             QWidget *pWidget) override;

  // Drag
  void dragEnterEvent(QGraphicsSceneDragDropEvent *pEvent) override;
  void dragLeaveEvent(QGraphicsSceneDragDropEvent *pEvent) override;
  void dropEvent(QGraphicsSceneDragDropEvent *pEvent) override;

  // Hover
  void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
  void hoverMoveEvent(QGraphicsSceneHoverEvent *event) override;
  void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;

  // Mouse press
  void mousePressEvent(QGraphicsSceneMouseEvent *pEvent) override;
  void mouseMoveEvent(QGraphicsSceneMouseEvent *pEvent) override;
  void mouseReleaseEvent(QGraphicsSceneMouseEvent *pEvent) override;
  QVariant itemChange(GraphicsItemChange change,
                      const QVariant &value) override;

 private:
  bool m_dropIndicator = false;
  QRectF m_renderRect;
  QRectF m_boundingRect;
  DragMode m_dragMode = DragMode::none;

  //
  QCursor cursor(const QPointF &pos) const;
  DragMode dragMode(const QPointF &pos) const;
  void drag(QPointF to);
};
}  // namespace sd