#pragma once

#include <QAbstractItemModel>
#include <QGraphicsScene>
#include <QMainWindow>

namespace sd {
class MainWindow : public QMainWindow {
  Q_OBJECT
 public:
  explicit MainWindow(QAbstractItemModel& toolboxModel, QGraphicsScene& scene,
                      QWidget* pParent);
};
}  // namespace sd