find_package(Qt5 COMPONENTS Gui Core Widgets REQUIRED)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

file(GLOB_RECURSE src CONFIGURE_DEPENDS "*.h" "*.hpp" "*.cpp")

add_library(sd_gui ${src} ${ui_src})
target_include_directories (sd_gui PRIVATE source 
                                   PUBLIC include)
target_link_libraries(sd_gui Qt5::Core Qt5::Gui Qt5::Widgets)
target_compile_features(sd_gui PUBLIC cxx_std_20)
