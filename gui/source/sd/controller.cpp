#include "sd/controller.h"

#include <QGraphicsScene>

namespace sd {
namespace {
constexpr qreal default_state_width = 180.0;
constexpr qreal default_state_left = -default_state_width / 2.0;
constexpr qreal default_state_height = 100.0;
constexpr qreal default_state_top = -default_state_height / 2.0;
constexpr QRectF default_state_rect(default_state_left, default_state_top,
                                    default_state_width, default_state_height);
class AddState : public QUndoCommand {
 public:
  AddState(StateGraphicsItem* pParent, QPointF point)
      : m_pParent(pParent), m_point(std::move(point)) {
    m_pState = new sd::StateGraphicsItem(default_state_rect, nullptr);
    m_pState->setPos(point);
  }

  ~AddState() {
    if (!m_pState->parentItem()) {
      delete m_pState;
    }
  }

  void undo() override {
    m_pState->setParentItem(nullptr);
    m_pState->scene()->removeItem(m_pState);
  }
  void redo() override { m_pState->setParentItem(m_pParent); }

 private:
  StateGraphicsItem* m_pParent;
  sd::StateGraphicsItem* m_pState;
  QPointF m_point;
};

class AddRootState : public QUndoCommand {
 public:
  AddRootState(QGraphicsScene& scene, QPointF point)
      : m_scene(scene), m_point(std::move(point)) {
    m_pState = new sd::StateGraphicsItem(default_state_rect, nullptr);
    m_pState->setPos(point);
  }

  ~AddRootState() {
    if (!m_pState->scene()) {
      delete m_pState;
    }
  }
  void undo() override { m_scene.removeItem(m_pState); }
  void redo() override { m_scene.addItem(m_pState); }

 private:
  QGraphicsScene& m_scene;
  sd::StateGraphicsItem* m_pState;
  QPointF m_point;
};
}  // namespace
Controller::Controller(QObject* pParent, QGraphicsScene& scene)
    : QObject(pParent), m_scene(scene) {}
void Controller::undo() { m_undoRedo.undo(); }
void Controller::redo() { m_undoRedo.redo(); }
void Controller::addState(StateGraphicsItem* pParent, QPointF pos) {
  if (pParent == nullptr) {
    m_undoRedo.push(new AddRootState(m_scene, pos));
  } else {
    m_undoRedo.push(new AddState(pParent, pos));
  }
}
}  // namespace sd