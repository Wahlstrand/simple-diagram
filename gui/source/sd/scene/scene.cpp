#include "sd/scene/scene.h"

#include <QGraphicsSceneDragDropEvent>
#include <QKeyEvent>
#include <QMimeData>
#include <iostream>

namespace sd {
Scene::Scene(QObject *pParent) : QGraphicsScene(pParent) {
  setSceneRect(0, 0, 800, 600);
}

// drag-drop
void Scene::dragEnterEvent(QGraphicsSceneDragDropEvent *pEvent) {
  QGraphicsScene::dragEnterEvent(pEvent);
}
void Scene::dragLeaveEvent(QGraphicsSceneDragDropEvent *pEvent) {
  QGraphicsScene::dragLeaveEvent(pEvent);
}
void Scene::dragMoveEvent(QGraphicsSceneDragDropEvent *pEvent) {
  QGraphicsScene::dragMoveEvent(pEvent);
}
void Scene::dropEvent(QGraphicsSceneDragDropEvent *pEvent) {
  const QMimeData *pData = pEvent->mimeData();
  if (!pData->hasText()) {
    return;
  }

  const QPointF &dropPos = pEvent->scenePos();

  QGraphicsItem *pDropTarget = itemAt(dropPos, QTransform());
  if (pDropTarget) {
    std::cout << "Target " << pDropTarget << std::endl;
  } else {
    std::cout << "Nothing " << pDropTarget << std::endl;
  }

  QGraphicsScene::dropEvent(pEvent);
}

void Scene::keyPressEvent(QKeyEvent *pKeyEvent) {
  if (pKeyEvent->matches(QKeySequence::Undo)) {
    emit undo();
  } else if (pKeyEvent->matches(QKeySequence::Redo)) {
    emit redo();
  }
}

}  // namespace sd