#include "sd/scene/state_graphics_item.h"

#include <QCursor>
#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <Qt>
#include <cmath>

namespace sd {
namespace {
constexpr qreal default_line_width = 2.0;
constexpr qreal drop_indicator_line_width = 4.0;
constexpr qreal selected_line_width = 6.0;
constexpr qreal stupid_extra_margin =
    20.0;  // Workaround for artifacts. Rendering SHOULD be
           // restricted to inside boundign rect as-is but for
           // some reason it is still glitchy. Grow boundign rect
           // some extra. This fixes most of the artifacts - but not all.
constexpr qreal margin = selected_line_width / 2.0 + stupid_extra_margin;

constexpr qreal min_render_width = 80.0;
constexpr qreal min_render_height = 60.0;

constexpr qreal corner_radius = 20.0;
constexpr qreal corner_grab_distance = 20.0;

qreal distance(const QPointF &p1, const QPointF &p2) {
  QPointF diff = p1 - p2;
  return std::sqrt(diff.x() * diff.x() + diff.y() * diff.y());
}
bool withinResizeGrabDistance(const QPointF &p1, const QPointF &p2) {
  return distance(p1, p2) < corner_grab_distance;
}
QRectF renderToBounding(const QRectF &render) {
  return render.marginsAdded(QMarginsF(margin, margin, margin, margin));
}
QRectF limitShrink(QRectF newRenderRect, DragMode mode) {
  const qreal newRenderWidth = newRenderRect.width();
  const qreal newRenderHeight = newRenderRect.height();

  if (newRenderWidth < min_render_width) {
    if (mode == DragMode::top_left || mode == DragMode::bottom_left) {
      newRenderRect.adjust(newRenderWidth - min_render_width, 0.0, 0.0, 0.0);
    } else if (mode == DragMode::top_right || mode == DragMode::bottom_right) {
      newRenderRect.adjust(0.0, 0.0, min_render_width - newRenderWidth, 0.0);
    }
  }

  if (newRenderHeight < min_render_height) {
    if (mode == DragMode::top_left || mode == DragMode::top_right) {
      newRenderRect.adjust(0.0, newRenderHeight - min_render_height, 0.0, 0.0);
    } else if (mode == DragMode::bottom_left ||
               mode == DragMode::bottom_right) {
      newRenderRect.adjust(0.0, 0.0, 0.0, min_render_height - newRenderHeight);
    }
  }

  return newRenderRect;
}

}  // namespace

StateGraphicsItem::StateGraphicsItem(QRectF renderRect,
                                     QGraphicsObject *pParent)
    : QGraphicsObject(pParent),
      m_renderRect(std::move(renderRect)),
      m_boundingRect(renderToBounding(m_renderRect)) {
  setAcceptDrops(true);
  setAcceptHoverEvents(true);
  setFlag(QGraphicsItem::ItemIsSelectable);
  setFlag(QGraphicsItem::ItemIsMovable);
  setFlag(QGraphicsItem::ItemSendsGeometryChanges);
}

QRectF StateGraphicsItem::boundingRect() const { return m_boundingRect; }
void StateGraphicsItem::paint(QPainter *pPainter,
                              const QStyleOptionGraphicsItem *option,
                              QWidget *pWidget) {
  pPainter->save();
  QPen pen = pPainter->pen();
  const qreal linewidth =
      isSelected()
          ? selected_line_width
          : (m_dropIndicator ? drop_indicator_line_width : default_line_width);
  pen.setWidthF(linewidth);
  pPainter->setPen(pen);
  pPainter->drawRoundedRect(m_renderRect, corner_radius, corner_radius);
  pPainter->restore();
}
void StateGraphicsItem::dragEnterEvent(QGraphicsSceneDragDropEvent *pEvent) {
  m_dropIndicator = true;
  update();
  QGraphicsObject::dragEnterEvent(pEvent);
}
void StateGraphicsItem::dragLeaveEvent(QGraphicsSceneDragDropEvent *pEvent) {
  m_dropIndicator = false;
  update();
  QGraphicsObject::dragLeaveEvent(pEvent);
}
void StateGraphicsItem::dropEvent(QGraphicsSceneDragDropEvent *pEvent) {
  m_dropIndicator = false;
  update();
  QGraphicsObject::dropEvent(pEvent);
}

void StateGraphicsItem::hoverEnterEvent(QGraphicsSceneHoverEvent *pEvent) {
  return QGraphicsObject::hoverEnterEvent(pEvent);
}
void StateGraphicsItem::hoverMoveEvent(QGraphicsSceneHoverEvent *pEvent) {
  setCursor(cursor(pEvent->pos()));
}
void StateGraphicsItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *pEvent) {
  return QGraphicsObject::hoverLeaveEvent(pEvent);
}

QVariant StateGraphicsItem::itemChange(GraphicsItemChange change,
                                       const QVariant &value) {
  if (change == QGraphicsItem::ItemPositionChange && parentItem()) {
    // Keep inside parent's bounding box
    const QRectF parentBoundary = parentItem()->boundingRect();

    const QPointF newPosCandidate = value.toPointF();
    const QRectF newRectCandidate =
        mapRectToParent(boundingRect().translated(newPosCandidate));
    const qreal halfWidth = newRectCandidate.width() / 2;
    const qreal halfHeight = newRectCandidate.height() / 2;

    return QPointF(
        std::clamp(newPosCandidate.x(), parentBoundary.left() + halfWidth,
                   parentBoundary.right() - halfWidth),
        std::clamp(newPosCandidate.y(), parentBoundary.top() + halfHeight,
                   parentBoundary.bottom() - halfHeight));
  } else if (change == QGraphicsItem::ItemSelectedHasChanged) {
    update();
  }
  return QGraphicsObject::itemChange(change, value);
}
void StateGraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent *pEvent) {
  m_dragMode = dragMode(pEvent->pos());
  if (m_dragMode == DragMode::none) {
    return QGraphicsObject::mousePressEvent(pEvent);
  }
}
void StateGraphicsItem::mouseMoveEvent(QGraphicsSceneMouseEvent *pEvent) {
  if (m_dragMode != DragMode::none) {
    drag(pEvent->pos());
  } else {
    return QGraphicsObject::mouseMoveEvent(pEvent);
  }
}
void StateGraphicsItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *pEvent) {
  m_dragMode = DragMode::none;
  return QGraphicsObject::mouseReleaseEvent(pEvent);
}

//
QCursor StateGraphicsItem::cursor(const QPointF &pos) const {
  if (withinResizeGrabDistance(pos, m_renderRect.bottomRight())) {
    return Qt::SizeFDiagCursor;
  } else if (withinResizeGrabDistance(pos, m_renderRect.topLeft())) {
    return Qt::SizeFDiagCursor;
  } else if (withinResizeGrabDistance(pos, m_renderRect.topRight())) {
    return Qt::SizeBDiagCursor;
  } else if (withinResizeGrabDistance(pos, m_renderRect.bottomLeft())) {
    return Qt::SizeBDiagCursor;
  } else {
    return Qt::ArrowCursor;
  }
}

DragMode StateGraphicsItem::dragMode(const QPointF &pos) const {
  if (withinResizeGrabDistance(pos, m_renderRect.bottomRight())) {
    return DragMode::bottom_right;
  } else if (withinResizeGrabDistance(pos, m_renderRect.topLeft())) {
    return DragMode::top_left;
  } else if (withinResizeGrabDistance(pos, m_renderRect.topRight())) {
    return DragMode::top_right;
  } else if (withinResizeGrabDistance(pos, m_renderRect.bottomLeft())) {
    return DragMode::bottom_left;
  } else {
    return DragMode::none;
  }
}

void StateGraphicsItem::drag(QPointF to) {
  prepareGeometryChange();
  switch (m_dragMode) {
    case DragMode::bottom_left:
      m_renderRect.setBottomLeft(to);
      break;
    case DragMode::top_right:
      m_renderRect.setTopRight(to);
      break;
    case DragMode::bottom_right:
      m_renderRect.setBottomRight(to);
      break;
    case DragMode::top_left:
      m_renderRect.setTopLeft(to);
      break;
    case DragMode::none:
      return;
  }

  m_renderRect = limitShrink(std::move(m_renderRect), m_dragMode);
  m_boundingRect = renderToBounding(m_renderRect);
}

}  // namespace sd