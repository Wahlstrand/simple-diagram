#include "sd/widgets/main_window.h"

#include "ui_main_window.h"

namespace sd {

MainWindow::MainWindow(QAbstractItemModel& toolboxModel, QGraphicsScene& scene,
                       QWidget* pParent)
    : QMainWindow(pParent) {
  Ui_MainWindow ui;
  ui.setupUi(this);

  ui.pToolboxView->setModel(&toolboxModel);
  ui.pGraphicsView->setScene(&scene);
}

}  // namespace sd