#include "sd/models/toolbox_model.h"

#include <QMimeData>
#include <iostream>

namespace sd {

ToolboxModel::ToolboxModel(QObject *pParent)
    : QAbstractListModel(pParent), m_actions({"State", "Comment"}) {}

int ToolboxModel::rowCount(const QModelIndex &parent) const {
  return static_cast<int>(m_actions.size());
}
int ToolboxModel::columnCount(const QModelIndex &parent) const { return 1; }
QVariant ToolboxModel::data(const QModelIndex &index, int role) const {
  switch (role) {
    case Qt::DisplayRole:
      return m_actions[static_cast<size_t>(index.row())];
  }
  return QVariant();
}
Qt::ItemFlags ToolboxModel::flags(const QModelIndex &index) const {
  return Qt::ItemIsDragEnabled | QAbstractListModel::flags(index);
}

Qt::DropActions ToolboxModel::supportedDragActions() const {
  return Qt::CopyAction;
}
QMimeData *ToolboxModel::mimeData(const QModelIndexList &indexes) const {
  if (indexes.size() != 1u) {
    return nullptr;
  }
  const QModelIndex &index = indexes.front();
  QMimeData *pData = new QMimeData();
  pData->setText(m_actions[index.row()]);
  return pData;
}

}  // namespace sd